package com.eldridge.euler.problem19;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;



public class Node
{                           
    int days = 0;
    public int counter = 0;
    private int year = 1900;
    
    public Node(){
       justDoIt();
    }
    
    private void justDoIt(){ 
        while(year < 2001){
            for(int j = 1; j <=12; j++){
                if((days + 1) % 7 == 0 && year > 1900){
                    counter++;
                }
                days += addMonth(j);
            }
            year++;
        }
    }
    
    private int addMonth(int m){
        int days = 0;
        switch (m) {
        case 1: 
          days = 31;
          break;
        case 2: 
            if(year % 4 == 0){
                if(year % 100 == 0 && year % 400 != 0){
                    days = 28;
                }else{
                    days = 29;
                }
            }else{
                days = 28;
            }
          break;
        case 3: 
          days = 31;
          break;
        case 4: 
          days = 30;
          break;
        case 5: 
          days = 31;
          break;
        case 6: 
          days = 30;
          break;
        case 7: 
          days = 31;
          break;
        case 8: 
          days = 31;
          break;
        case 9: 
          days = 30;
          break;
        case 10: 
          days = 31;
          break;
        case 11: 
          days = 30;
          break;
        case 12: 
          days = 31;
          break;
        default: 
          System.out.println("0 passed to addMonth");
          days = 0;
      }
      return days;
    }    
}
