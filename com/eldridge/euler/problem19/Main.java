package com.eldridge.euler.problem19;

public class Main
{
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis(); 
        Node bam = new Node();
        System.out.println(bam.counter);
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
}