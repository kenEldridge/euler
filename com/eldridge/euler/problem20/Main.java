package com.eldridge.euler.problem20;

import java.math.BigInteger;

public class Main
{
   
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis();
        
        BigInteger n = BigInteger.ZERO;
        int sum = 0;
        n = factorial(100);
        String num = n.toString();
        int length = num.length();
        for(int i = 0; i < length; i++){
            sum += Integer.parseInt(num.substring(0, 1).toString());
            num = num.substring(1, num.length());
        }
        System.out.println(sum);
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
    
    public static BigInteger factorial(int n)
    {
        BigInteger ret = BigInteger.ONE;
        for (int i = 1; i <= n; ++i) ret = ret.multiply(BigInteger.valueOf(i));
        return ret;
    }
    
}