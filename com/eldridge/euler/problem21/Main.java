package com.eldridge.euler.problem21;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.eldridge.euler.util.Helper;

public class Main
{
    //private static HashMap<Long, Integer> map = new HashMap<Long, Integer>();
    private static ArrayList<Pair> pairs = new ArrayList<Pair>();
    
    
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis();
        //HashMap<Long, Integer> map2 = new HashMap<Long, Integer>();
        
        int size = 10000;
        
        
        ArrayList<Long> amicableNumber = new ArrayList<Long>();
        long[] values = loadList(size);
        //map2 = map;
        
        for(int i = 0; i < pairs.size(); i++){
            if(Arrays.binarySearch(values, pairs.get(i).a) >= 0){     
                
            }
        }
       
        
        System.out.println(Helper.sumArrayList(amicableNumber));
        
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
    
    private static long[] loadList(int size){
        long[] list = new long[size + 1];
        for(int j = 0; j < size; j++){
           list[j] = Helper.sumArrayList(Helper.findProperDivisors(j));
           pairs.add(new Pair(j, list[j]));
        }
        Arrays.sort(list);
        return list;
    }
    
    private static void printN(Long n, Map map){
        System.out.println(map.get(n));
    }
    
}

