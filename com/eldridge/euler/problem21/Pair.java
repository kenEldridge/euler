package com.eldridge.euler.problem21;

public class Pair{
    
    public long a = 0;
    public long b = 0;
    
    Pair(long a, long b){
        this.a = a;
        this.b = b;
    }
    
}