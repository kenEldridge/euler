package com.eldridge.euler.problem18;

public class Main
{
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis(); 
        Node bam = new Node();
        System.out.println("Max sum: " + bam.sum);
        System.out.println("Iterations: " + bam.counter);
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
}