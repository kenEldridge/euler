package com.eldridge.euler.problem18;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;



public class Node
{                           
    ArrayList<ArrayList<Integer>> tri = new ArrayList<ArrayList<Integer>>();
    public static int counter = 0;
    public static int sum = 0;
    
    public Node(){
       loadIt("triangle.txt");
       sum = inspect();
    }
    
    private int inspect(){        
        int rowNum = tri.size() - 2;
        int temp = 0;
        int temp2 = 0;
        int offset = 0;
        
        for(int i = rowNum; i >= 0; i--){
            for(int j = 0; j < tri.get(i).size(); j++){
                counter++;
                if(tri.get(i + 1).get(j) > tri.get(i + 1).get(j + 1)){
                    offset = 0;
                }else{
                    offset = 1;
                }
                temp = tri.get(i + 1).get(j + offset);
                temp2 = tri.get(i).get(j);
                tri.get(i).set(j, temp + temp2);
            }
        }
            
        return tri.get(0).get(0);
    }
    
    private void loadIt(String name) {
        File file = new File(name);
        BufferedReader reader = null;
        try {

            reader = new BufferedReader(new FileReader(file));

            String text = null;

            // repeat until all lines are read

            while ((text = reader.readLine()) != null) {
                tri.add(new ArrayList<Integer>());
                String[] row = text.split(" ");
                for(int i = 0; i < row.length; i++){
                    tri.get(tri.size() - 1).add(Integer.parseInt(row[i]));
                }
            }

        } catch (FileNotFoundException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (reader != null) {

                    reader.close();

                }

            } catch (IOException e) {

                e.printStackTrace();

            }

        }
    }
    
}
