package com.eldridge.euler.problem23;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import com.eldridge.euler.util.Helper;


public class Main
{
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis();
        long total = 0L;
        ArrayList<Integer> abundantNums = new ArrayList<Integer>();
        HashMap<Integer, Boolean> numbers = new HashMap<Integer, Boolean>();
        
        for(int j = 0; j < 28123; j++){
            Long temp = Helper.sumArrayList(Helper.findProperDivisors(j));
            if(temp > j){
                abundantNums.add(j);
            }    
        }
        
        for(int j = 0; j < 28124; j++){
            numbers.put(j, true);
        }
        
        int temp = 0;
        for(int i = 0; i < abundantNums.size(); i++){
            for(int j = 0; j < abundantNums.size(); j++){
                temp = abundantNums.get(i) + abundantNums.get(j);
                numbers.put(temp, false);
                }
        }

        for(Integer key : numbers.keySet()){
            if(numbers.get(key)){
                total += key;
            }
        }

        System.out.println(total);
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
    
}

