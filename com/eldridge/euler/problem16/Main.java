package com.eldridge.euler.problem16;

public class Main
{
   
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis();
        String num = "10715086071862673209484250490600018105614048117055336074437503883703510511249361224931983788156958581275946729175531468251871452856923140435984577574698574803934567774824230985421074605062371141877954182153046474983581941267398767559165543946077062914571196477686542167660429831652624386837205668069376";
        String exp = "";
        int length = num.length();
        for(int i = 0; i < length; i++){
            exp = exp + num.substring(0, 1) + "+";
            num = num.substring(1, num.length());
            System.out.println();
        }
        System.out.println(exp);
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
    
}