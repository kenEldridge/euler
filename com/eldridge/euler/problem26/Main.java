package com.eldridge.euler.problem26;

import java.math.BigDecimal;



public class Main
{
    
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis();
        BigDecimal a = BigDecimal.ZERO;
        BigDecimal counter = BigDecimal.ONE;
        int bam = 0;
        for(int j = 0; j < 1000; j++){
           
           counter = counter.add(BigDecimal.ONE);
           try{
               a = (BigDecimal.ONE).divide(counter);
           }catch(Exception e){
               if(Integer.parseInt(counter.toString()) % 3 != 0){
                   bam++;
                   System.out.println(bam + ": " + counter);
               }
           }
                      
           //System.out.println(counter + ": " + a);
        }
           
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
    
}

