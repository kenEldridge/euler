package com.eldridge.euler.problem15;

import java.util.ArrayList;



public class Helper
{
    ArrayList<ArrayList<Node>> nodes = new ArrayList<ArrayList<Node>>();
    int size = 0;
    int index = 0;
    
    public Helper(int size){
        this.size = size + 1;
        populateNodesList();
        System.out.println(nodes.get(index).size());
    }
    
    private void populateNodesList(){
        nodes.add(new ArrayList<Node>());
        nodes.get(0).add(new Node(0,0));
        boolean finished = false;
        while(!finished){
            nodes.add(new ArrayList<Node>());
            for(int j = 0; j < nodes.get(0).size(); j++){
                finished = true;
                //For each Node: Is this a one, two or zero decision node?
                
                //Find next step(s) and add to nodes

                if(nodes.get(0).get(j).getM() < size - 1){
                    //can move down
                    nodes.get(1).add(new Node(nodes.get(0).get(j).getM() + 1, nodes.get(0).get(j).getN()));
                    finished = false;
                }
                if(nodes.get(0).get(j).getN() < size - 1){
                    //can move right
                    nodes.get(1).add(new Node(nodes.get(0).get(j).getM(), nodes.get(0).get(j).getN() + 1));
                    finished = false;
                }
                
                if(finished){
                    index = 0;
                }
            }
            if(finished){
                index = 0;
            }else{
                nodes.remove(0);
            }
        }
        System.out.println();
    }
}
