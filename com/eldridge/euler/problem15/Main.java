package com.eldridge.euler.problem15;

import com.eldridge.euler.problem15.Helper;

public class Main
{
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis(); 
        Helper helper = new Helper(3); 
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
}