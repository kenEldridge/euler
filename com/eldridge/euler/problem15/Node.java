package com.eldridge.euler.problem15;

public class Node
{
    private int m = 0;
    private int n = 0;
    
    public Node(){
        
    }
    
    public Node(int i, int j){
        m = i;
        n = j;
    }
    
    public int getM()
    {
        return m;
    }
    public void setM(int m)
    {
        this.m = m;
    }
    public int getN()
    {
        return n;
    }
    public void setN(int n)
    {
        this.n = n;
    }
    
}
