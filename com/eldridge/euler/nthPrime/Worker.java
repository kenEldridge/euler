package com.eldridge.euler.nthPrime;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;



public class Worker extends java.lang.Thread implements java.lang.Runnable
{
    String name;
    int n;
    int potentialPrime = 0;
    private static ArrayList<Integer> primes = new ArrayList<Integer>();
    private static Semaphore primeSemaphore = new Semaphore(1);
    private static Semaphore potentialPrimeSemaphore = new Semaphore(1);

    Worker(String name, int n){
        this.name = name;
        this.n = n;
        this.setPriority(NORM_PRIORITY);
        try
        {
            primeSemaphore.acquire();
            this.potentialPrime = 5;
            if(primes.size() == 0){
                primes.add(1);
                primes.add(3);
                primes.add(5);
            }
            primeSemaphore.release();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public void run()
    {
        getNthPrime();

    }

    void getNthPrime(){
        boolean prime = true;
        while(primes.size() < n){
            try
            {
                potentialPrimeSemaphore.acquire();
                potentialPrime+=2;
                potentialPrimeSemaphore.release();
                //find primes
                int temp = potentialPrime / 2;
                if(temp%2 == 0){
                    temp--;
                }
                while(temp > 1){
                    if(potentialPrime % temp-- == 0){
                        prime = false;
                        break;
                    }   
                }

                if(prime){
                    //store prime
                    primeSemaphore.acquire();
                    primes.add(potentialPrime);
                    primeSemaphore.release();	                
                }
                prime = true;
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        System.out.println(primes.get(n-1));
    }

}


