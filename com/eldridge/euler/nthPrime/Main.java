package com.eldridge.euler.nthPrime;


public class Main
{
    public static void main(String[] args)
	{
        long startTime = System.currentTimeMillis(); 
        
        
       
        try
        {
            int n = 38;
            Worker worker1 = new Worker("Worker 1", n);
            Worker worker2 = new Worker("Worker 2", n);
            Worker worker3 = new Worker("Worker 3", n);
            Worker worker4 = new Worker("Worker 4", n);
            worker1.start();
//            worker2.start();
//            worker3.start();
//            worker4.start();
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
       
        
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
	}
}