package com.eldridge.euler.problem25;

import java.math.BigInteger;


public class Main
{
    private static BigInteger a = BigInteger.ONE;
    private static BigInteger b = BigInteger.ONE;
    private static BigInteger temp = BigInteger.ZERO;
    private static int counter = 2;
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis();
        long total = 0L;
        
        while(b.toString().length() < 1000){
           nextFib();
           counter++;
        }
           
        System.out.println(counter);
    
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
    
    public static BigInteger nextFib(){
        temp = a.add(b);
        a = b;
        b = temp;
        return b;
    }
    
}

