package com.eldridge.euler.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;

public class Helper
{

    public static ArrayList<Long> findProperDivisors(long number)
    {
        ArrayList<Long> divisors = new ArrayList<Long>();
        Long divisor = 1L;
        Long high = number;
        while (divisor < high)
        {
            if (number % divisor == 0)
            {
                high = number / divisor;
                if (divisor != 1 && divisor != high)
                {
                    divisors.add(divisor);
                    divisors.add(high);
                }
                else
                {
                    divisors.add(divisor);
                }
            }
            divisor++;
        }
        return divisors;
    }

    public static Long sumArrayList(ArrayList<Long> list)
    {
        Long sum = 0L;
        for (int j = 0; j < list.size(); j++)
        {
            sum += list.get(j);
        }
        return sum;
    }

    public static void printArrayList(ArrayList<Long> list)
    {
        for (int j = 0; j < list.size(); j++)
        {
            System.out.print(list.get(j) + ", ");
        }
        System.out.println();
    }

    public static ArrayList<String> loadFile(String name)
    {
        ArrayList<String> list = new ArrayList<String>();
        File file = new File(name);
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new FileReader(file));
            String text = null;
            // repeat until all lines are read
            while ((text = reader.readLine()) != null)
            {
                list.add(text);
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (reader != null)
                {
                    reader.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return list;
    }

}