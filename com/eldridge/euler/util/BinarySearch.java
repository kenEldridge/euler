package com.eldridge.euler.util;

import java.util.AbstractMap;
import java.util.ArrayList;

public class BinarySearch{
    
    private Long n = 0L;
    private AbstractMap map;
    private ArrayList<Long> values;

    public BinarySearch(){
        values = (ArrayList<Long>) map.values();
    }
    
    public Long binarySearch(Long k){
        Long ret = 0L;
        
        while(k != n){
            
            if(values.get(Integer.parseInt((k).toString())) < n){
                ret = binarySearch(k / 2);
            }
            else{
                if(values.get(Integer.parseInt((k).toString())) == n){
                    ret = n;
                }
                ret = binarySearch(k + k / 2);
            }
        }
        
        return ret;
    }
    
    public void setMap(AbstractMap map)
    {
        this.map = map;
    }

    public void setN(Long n)
    {
        this.n = n;
    }
    
}