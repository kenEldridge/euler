package com.eldridge.euler.problem14;

import com.eldridge.euler.problem14.Helper;

public class Main
{
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis(); 
        Helper helper = new Helper(); 
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
}