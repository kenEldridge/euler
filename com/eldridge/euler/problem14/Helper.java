package com.eldridge.euler.problem14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class Helper
{
    Long largest = 0L;
    ArrayList<Long> chainSize = new ArrayList<Long>();
    ArrayList<Long> startingTerm = new ArrayList<Long>();
    
    public Helper()
    {
       runIt();    
       for(int j = 0; j < startingTerm.size(); j++){
           System.out.println("Chain size: " + chainSize.get(j) + ", the number: " + startingTerm.get(j));
       }
       
    }
    
    private void runIt(){
        Long number = 0L;
        while(number < 1000000){
            Long temp = buildChain(number);
            if(temp >= largest){
                largest = temp;
                chainSize.add(temp);
                startingTerm.add(number);
            }
            number++;
        }
    }
        
    private Long buildChain(Long number){
        ArrayList<Long> chain = new ArrayList<Long>();
        chain.add(number);
        while(number > 1){
            if(number % 2 == 0){
                number /= 2;
            }else{
                number = number * 3 + 1;
            }  
            chain.add(number);
        }
       if(chain.size() == 525){
            for(int j = 0; j < chain.size(); j++){
                System.out.print(chain.get(j) + ", ");
            }
            System.out.println();
        }
        
        
        return Long.parseLong(String.valueOf(chain.size()));
    }
    
}
