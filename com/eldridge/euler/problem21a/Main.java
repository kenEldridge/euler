package com.eldridge.euler.problem21a;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.eldridge.euler.util.Helper;

public class Main
{
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis();
        int size = 10000;
        ArrayList<Long> amicableNumber = new ArrayList<Long>();
        long j = 0;
        while (j < size)
        {
            long one = Helper.sumArrayList(Helper.findProperDivisors(j));
            long two = Helper.sumArrayList(Helper.findProperDivisors(one));
            if (j == two && j != one)
            {
                amicableNumber.add(j);
            }
            j++;
        }
        System.out.println(Helper.sumArrayList(amicableNumber));

        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
}
