package com.eldridge.euler.problem15a;


public class Node
{
    private int m = 0;
    private int n = 0;
    private static int size = 0;
    private static Long route = 0L;
    
    public Node(int i, int j){
        m = i;
        n = j;
        dive();
    }
    
    public void dive(){
        //For each Node: Is this a one, two or zero decision node?
        //Find next step(s) and add to nodes
        Node new1 = null;
        Node new2 = null;
        if(this.m < size - 1){
            //can move down
           new1 = new Node(this.m + 1, this.n);
        }
        if(this.n < size - 1){
           //can move right
            new2 = new Node(this.m, this.n + 1);
        }
        if(this.m == size - 1 && this.n == size -1){
            route++;
        }
    }

    public static void setSize(int size)
    {
        Node.size = size + 1;
    }

    public static Long getRoute()
    {
        return route;
    }
    
}
