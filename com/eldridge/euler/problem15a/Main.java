package com.eldridge.euler.problem15a;

public class Main
{
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis(); 
        Node.setSize(18);
        Node root = new Node(0,0);
        System.out.println(root.getRoute());
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
}