package com.eldridge.euler.problem15b;

public class Main
{
    private static int size = 20;
    private static Long routeCount = 0L;
    
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis();
        dive(0, 0);
        System.out.println(routeCount);
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
    
    public static void dive(int p, int q){
        if(p < size || q < size){
            if(p < size){
                //can move down
               dive(p + 1, q);
            }
            if(q < size){
               //can move right
               dive(p, q + 1);
            }
        }else
        {
            routeCount++;
        }
    }
}