package com.eldridge.euler.problem12;

import com.eldridge.euler.problem12.Helper;

public class Main
{
    public static void main(String[] args)
	{
        long startTime = System.currentTimeMillis(); Helper helper = new
        Helper(500); long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
	}
}