package com.eldridge.euler.problem12;

import java.util.ArrayList;
import java.util.List;


public class Helper
{
	ArrayList<Long> naturalNumbers = new ArrayList<Long>();
	int size = 0;
	
	public Helper(int size)
	{
		this.size = size;
		iterate();
        //binary();
	}
	
	public void iterate(){
		ArrayList<Long> divs = null;
        Long tri = 1L;
		Long k = 1L;
		do{
			divs = findDivisors(tri);
            k++;
            tri = tri + k;
            if(divs.size() % 50 == 0){
				System.out.println("Number of divisors found: " + divs.size() + " at iteration " + k);
			}
		}while(divs.size() < size);
		
		System.out.println(tri - k);
	}
	
    public void binary(){
        System.out.println("number of divisors: " + (findDivisors(76576500L)).size());
    }
    
	public ArrayList<Long> findDivisors(Long number)
	{
		naturalNumbers.add(number);
		ArrayList<Long> divisors = new ArrayList<Long>();
        Long divisor = 1L;
        Long high = number;
        while(divisor < high){
            if (number % divisor == 0) 
            {
                high = number / divisor;
                divisors.add(divisor);
                divisors.add(high);
            }
            divisor++;
        }   
		return divisors;
	}
	
}


