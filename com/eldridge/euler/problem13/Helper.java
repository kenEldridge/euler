package com.eldridge.euler.problem13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class Helper
{
    Long theSum = 0L;    
    ArrayList<Long> one = new ArrayList<Long>();
    ArrayList<Long> two = new ArrayList<Long>();
    ArrayList<Long> three = new ArrayList<Long>();
    ArrayList<Long> four = new ArrayList<Long>();
    Long oneSum = 0L;
    Long twoSum = 0L;
    Long threeSum = 0L;
    Long fourSum = 0L;
    
    public Helper()
    {
        loadIt("euler13.txt");
        putTogether();
        theSum = oneSum + twoSum + threeSum + fourSum;
        System.out.println(theSum.toString().substring(0, 10));        
    }
    
    private void putTogether(){
        
        fourSum = findSum(four);
        String fourSt =  fourSum.toString();
        fourSt = fourSt.substring(0, fourSt.length() - 11);
        fourSum = Long.parseLong(fourSt);
        
        threeSum = findSum(three);
        threeSum += fourSum;
        String threeSt =  threeSum.toString();
        threeSt = threeSt.substring(0, threeSt.length() - 13);
        threeSum = Long.parseLong(threeSt);
        
        twoSum = findSum(two);
        twoSum += threeSum;
        String twoSt =  twoSum.toString();
        twoSt = twoSt.substring(0, twoSt.length() - 13);
        twoSum = Long.parseLong(twoSt);
        
        oneSum = findSum(one);
        oneSum += twoSum;
        String oneSt =  oneSum.toString();
        oneSt = oneSt.substring(0, oneSt.length() - 13);
        fourSum = Long.parseLong(oneSt);
        
    }
    
    private Long findSum(ArrayList<Long> nums){
        Long sum = 0L;
        for(int i = 0; i < nums.size(); i++){
            sum = sum + nums.get(i);
        }
        return sum;
    }
    
    private void loadIt(String name) {
        File file = new File(name);
        BufferedReader reader = null;
        try {

            reader = new BufferedReader(new FileReader(file));

            String text = null;

            // repeat until all lines is read

            while ((text = reader.readLine()) != null) {
                one.add(Long.parseLong(text.substring(0, 13)));
                two.add(Long.parseLong(text.substring(13, 26)));
                three.add(Long.parseLong(text.substring(26, 39)));
                four.add(Long.parseLong(text.substring(39, 50)));
            }

        } catch (FileNotFoundException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (reader != null) {

                    reader.close();

                }

            } catch (IOException e) {

                e.printStackTrace();

            }

        }
    }
    
}
