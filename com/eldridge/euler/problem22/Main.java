package com.eldridge.euler.problem22;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Main
{
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis();
        long total = 0L;
        ArrayList<String> names = loadFile("names.txt");
        HashMap<String, Integer> alpha = loadAlpha();
        Collections.sort(names);
        for (String key : alpha.keySet()) {
            System.out.println("key/value: " + key + "/"+alpha.get(key));
        }
        for(int j = 0; j < names.size(); j++){
            total += (j + 1) * findNameVal(names.get(j), alpha);
        }
        System.out.println(total);
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
    
    private static int findNameVal(String name, HashMap<String, Integer> map){
        int sum = 0;
        for(int i = 0; i < name.length(); i++){
            sum += map.get(String.valueOf(name.charAt(i)));
        }
        return sum;
    }
    
    private static HashMap<String, Integer> loadAlpha(){
        HashMap<String, Integer> alphaG = new HashMap<String, Integer>();
        String alp = "_ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for(int j = 0; j < alp.length(); j++){
            alphaG.put(String.valueOf(alp.charAt(j)), j);
        }
        return alphaG;
    }
    
    private static ArrayList<String> loadFile(String name)
    {
        ArrayList<String> list = new ArrayList<String>();
        File file = new File(name);
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new FileReader(file));
            String text = null;
            // repeat until all lines are read
            while ((text = reader.readLine()) != null)
            {
                text = text.substring(1, text.length());
                text = text.substring(0, text.length() - 1);
                String[] names = text.split("\",\"");
                for(int j = 0; j < names.length; j++){
                    list.add(names[j]);
                }
            }
            
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (reader != null)
                {
                    reader.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return list;
    }
}

