package com.eldridge.euler.problem17;

public class Main
{
    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis(); 
        Node bam = new Node();
        bam.buildIt(0);
        long endTime = System.currentTimeMillis();
        System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
    }
}