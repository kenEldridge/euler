package com.eldridge.euler.problem17;

import java.math.BigInteger;


public class Node
{                           //  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18  19 20
    private final int[] nums = {0, 3, 3, 5, 4, 4, 3, 5, 5, 4, 3, 6, 6, 8, 8, 7, 7, 9, 8, 8, 6};
    private final int[] tens = {0, 0, 6, 6, 5, 5, 5, 7, 6, 6};
    private final int thou = 8;
    private final int hund = 7;
    private final int and = 3;
    private static Long counter = 0L;
    private int theNumber = 1000;
    
    public Node(){
       
    }
    
    public void buildIt(int num){
        theNumber = num;
        int yetAnotherPoorlyNamedCounter = 0;
        while(theNumber <= 1000){            
            Long localCounter = counter;
            num = theNumber;
            int temp = 0;
            boolean andNeeded = false;
            temp = num / 1000;
            if(temp > 0){
                counter = counter + nums[temp];
                counter = counter + thou;
            }
            num = num % 1000;
            
            temp = num / 100;
            if(temp > 0){
                counter = counter + nums[temp];
                counter = counter + hund;
                if(num % 100 != 0){
                    andNeeded = true;
                }
            }      
            num = num % 100;
            
            if(num > 0 && andNeeded){
                counter = counter + and;
            }
            
            temp = num / 10;
            if(temp > 1 && num != 20){
                counter = counter + tens[temp];
            }else{
                counter = counter + nums[num];
                num = 0;
            }
            num = num % 10;
            
            if(num > 0){
                counter = counter + nums[num];
            }
            System.out.println(yetAnotherPoorlyNamedCounter + ": " + (counter - localCounter));
            yetAnotherPoorlyNamedCounter++;
            theNumber++;
        }
        System.out.println(counter);
    }
}
